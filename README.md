# GIT DOC
## a short document on the most famous file version management system
`git add` -> make file followable and add file to the zone index<br>
`git commit` -> validate modifications and add file to the git repository<br>
`git commit -a` -> place all followed files directly into the git repository (without add command)<br>
`git clone [newName]` -> get a copy of repo : create new folder and copy file from distant repo. Create a .git folder inside.<br>
`git status`<br>
`git diff` -> inspect modifications <br>
`git rm` -> delete file from index zone<br>
`git mv` -> rename file<br>
`git log` -> show history<br>

-- cancel actions
`git commit --amend` -> the last commit is used for the current commit

-- branch
`git branch name-of-branch` -> create a new branch : a pointer on the last commit (the current branch and the new created branch point on the same commit at this point)  
`git chechout name-of-branch` -> switch branch
